#!/bin/env php
<?php

if (php_sapi_name() !== 'cli') {
    die('This script must be called from command line.');
}


$app_path = realpath(__DIR__ . '/..');

require $app_path . '/common.php';
require $app_path . '/kanboard/vendor/autoload.php';


/**
 * Return the list of emails contained in given file, and truncate the list.
 *
 * @param string $path
 *
 * @return string[] The list of emails
 */
function fetch_waiting_emails($path) {
    $file = @fopen($path, 'r+');
    if (!$file) {
        touch($path);
        return [];
    }

    // we need to obtain an exclusive lock to avoid an email being inserted
    // during the process, especially before truncating.
    if (!flock($file, LOCK_EX)) {
        fclose($file);
        return [];
    }

    $filesize = filesize($path);
    if ($filesize <= 0) {
        flock($file, LOCK_UN);
        fclose($file);
        return [];
    }

    $emails_txt = fread($file, $filesize);
    if ($emails_txt === false) {
        flock($file, LOCK_UN);
        fclose($file);
        return [];
    }

    ftruncate($file, 0);

    flock($file, LOCK_UN);
    fclose($file);

    $emails = explode("\n", $emails_txt);
    return $emails;
}


/**
 * Return a PDO connection for the given Framaboard account.
 *
 * @param string $account_name
 *
 * @return \PDO
 */
function pdo_for_account($account_name) {
    $db_path = join_path(PATH_ACCOUNTS, $account_name, 'db.sqlite');
    $dsn = 'sqlite:' . $db_path;
    $options = [
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES => false,
    ];
    return new \PDO($dsn, null, null, $options);
}


/**
 * Search a list of usernames by emails
 *
 * @param \PDO $pdo a PDO connection to a Kanboard database
 * @param string[] $emails
 *
 * @return string[]|boolean A list of corresponding usernames or false on failure
 */
function list_usernames_by_emails($pdo, $emails) {
    $in_query = implode(',', array_fill(0, count($emails), '?'));
    $sql = 'SELECT LOWER(email), username FROM users '
        . "WHERE email COLLATE NOCASE IN ({$in_query})"
        . 'GROUP BY email';

    $statement = $pdo->prepare($sql);
    if ($statement === false) {
        return false;
    }

    $result = $statement->execute($emails);
    if ($result === false) {
        return false;
    }

    return $statement->fetchAll(\PDO::FETCH_GROUP | \PDO::FETCH_COLUMN);
}


$emails = fetch_waiting_emails(PATH_LOST_ACCOUNT_EMAILS);
$emails = array_filter($emails, function ($email) {
    // emails are validated before being inserted in the file, but we want to
    // be sure the admin didn't inserted an invalid value.
    return filter_var($email, FILTER_VALIDATE_EMAIL);
});

if ($emails) {
    $accounts = list_accounts();
    $accounts_by_emails = [];

    foreach ($accounts as $account) {
        $account_name = $account['name'];

        try {
            $pdo = pdo_for_account($account_name);
        } catch (\PDOException $e) {
            syslog(
                LOG_WARNING,
                "An error occured during `{$account_name}` account database initialization: {$e->getMessage()}."
            );
            continue;
        }

        $usernames_by_emails = list_usernames_by_emails($pdo, $emails);

        if ($usernames_by_emails === false) {
            syslog(
                LOG_WARNING,
                "A SQL error occured when listing usernames for `{$account_name}` account."
            );
            continue;
        }

        foreach ($usernames_by_emails as $email => $usernames) {
            if (!isset($accounts_by_emails[$email])) {
                $accounts_by_emails[$email] = [];
            }

            $accounts_by_emails[$email][$account_name] = $usernames;
        }
    }

    foreach ($accounts_by_emails as $email => $usernames_by_accounts) {
        $subject = 'Vos comptes Framaboard';

        $html = '<p>Bonjour,</p>';
        $html .= '<p>Voici la liste des comptes et instances relatives à cette adresse courriel&nbsp;:</p>';
        $html .= '<ul>';
        foreach ($usernames_by_accounts as $account => $usernames) {
            $account_url = 'https://' . $account . '.' . URL_BASE;
            foreach ($usernames as $username) {
                $html .= "<li>{$username} sur l’espace <a href=\"{$account_url}\">{$account_url}</a></li>";
            }
        }
        $html .= '</ul>';
        $html .= '<p><strong>Attention&nbsp;:</strong> Framaboard est <strong>sensible à la casse</strong> (c’est-à-dire qu’il distingue les lettres minuscules des lettres majuscules).</p>';
        $html .= '<p>Si vous avez oublié votre mot de passe, vous pourrez le réinitialiser à partir de votre identifiant sur la page «&nbsp;Mot de passe oublié&nbsp;?&nbsp;»</p>';
        $html .= '<p>Bonne journée&nbsp;!</p>';
        $html .= '<p><i>PS&nbsp;: ce courriel a été envoyé parce que vous –&nbsp;ou quelqu’un d’autre&nbsp;– avez demandé la récupération des espaces de travail auxquels vous êtes inscrit·e, ainsi que vos identifiants de connexion.</i></p>';

        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom(SMTP_FROM, SMTP_AUTHOR)
            ->setTo([$email]);

        $headers = $message->getHeaders();
        $headers->addTextHeader('Auto-Submitted', 'auto-generated');
        $message->setBody($html, 'text/html');

        $transport = Swift_MailTransport::newInstance();
        Swift_Mailer::newInstance($transport)->send($message);
    }
}

<?php

require '../common.php';

$success = '';
$error = '';
$email = '';

if (!empty($_POST)) {
    $email = $_POST['email'];

    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        file_put_contents(
            PATH_LOST_ACCOUNT_EMAILS,
            $email . PHP_EOL,
            FILE_APPEND | LOCK_EX
        );

        $email = '';
        $success = '<strong>Un courriel va vous être envoyé dans quelques instants si nous trouvons des comptes liés à votre adresse.</strong> Un certain temps (de l’ordre de quelques minutes) peut s’écouler avant l’envoi du courriel&nbsp;; si vous ne le recevez pas immédiatement, veuillez attendre un petit peu puis vérifier dans vos spams.';
    } else {
        $error = 'L’adresse courriel que vous avez entrée est invalide.';
    }
}

include 'views/perdu.phtml';

<?php

define('ENVIRONMENT', isset($_ENV['APP_ENVIRONMENT']) ? $_ENV['APP_ENVIRONMENT'] : 'production');
define('URL_BASE', isset($_ENV['APP_URL_BASE']) ? $_ENV['APP_URL_BASE'] : 'framaboard.org');
define('TITLE', isset($_ENV['APP_TITLE']) ? $_ENV['APP_TITLE'] : 'Framaboard');
define(
    'LANGUAGE_DEFAULT',
    isset($_ENV['APP_LANGUAGE_DEFAULT']) ? $_ENV['APP_LANGUAGE_DEFAULT'] : 'fr_FR'
);
define(
    'SMTP_FROM',
    isset($_ENV['APP_SMTP_FROM']) ? $_ENV['APP_SMTP_FROM'] : 'noreply@framaboard.org'
);
define(
    'SMTP_AUTHOR',
    isset($_ENV['APP_SMTP_AUTHOR']) ? $_ENV['APP_SMTP_AUTHOR'] : 'Framasoft'
);
define(
    'APP_OPENED',
    isset($_ENV['APP_OPENED']) ? filter_var($_ENV['APP_OPENED'], FILTER_VALIDATE_BOOLEAN) : true
);

/**
 * Build a directory path by concatenating a list of directory names.
 *
 * You can call this function with as many params as you want.
 * Example: join_path('home', 'test', 'Downloads') returns home/test/Downloads
 *
 * @param string [...] a list of directory names
 * @return a string corresponding to the final pathname
 */
function join_path() {
    $path_parts = func_get_args();
    return join(DIRECTORY_SEPARATOR, $path_parts);
}

/**
 * Return if a string starts with a given string.
 *
 * @param string $haystack The string to test.
 * @param string $needle   The substring to test.
 *
 * @return bool True if $haystack starts with $needle, false otherwise.
 */
function starts_with($haystack, $needle)
{
    return $needle === '' ||
            strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

/**
 * Return if a string ends with a given string.
 *
 * @param string $haystack The string to test.
 * @param string $needle   The substring to test.
 *
 * @return bool True if $haystack ends with $needle, false otherwise.
 */
function ends_with($haystack, $needle)
{
    $index_start = strlen($haystack) - strlen($needle);
    return $needle === '' || ($index_start >= 0 &&
            strrpos($haystack, $needle, $index_start) !== false);
}

/**
 * Delete dir recursively.
 *
 * @param string $dir Path dir to delete.
 *
 * @return boolean True if success, false otherwise.
 */
function rm_rf($dir)
{
    $files = array_diff(scandir($dir), array('.', '..'));
    foreach ($files as $file) {
        $path = join_path($dir, $file);
        if (is_dir($path)) {
            rm_rf($path);
        } else {
            unlink($path);
        }
    }
    return rmdir($dir);
}

/**
 * Initialize and return a container similar to Kanboard one.
 *
 * @param string $subdomain subdomain to find db path.
 * @return Pimple\Container a container used to create user database.
 */
function get_kanboard_container($subdomain) {
    defined('DEBUG') || define('DEBUG', false);
    defined('DB_DRIVER') || define('DB_DRIVER', 'sqlite');
    defined('DB_FILENAME') || define('DB_FILENAME', join_path(PATH_ACCOUNTS, $subdomain, 'db.sqlite'));
    defined('DB_RUN_MIGRATIONS') || define('DB_RUN_MIGRATIONS', true);

    defined('REVERSE_PROXY_AUTH') || define('REVERSE_PROXY_AUTH', false);
    defined('LDAP_AUTH') || define('LDAP_AUTH', false);
    defined('GITLAB_AUTH') || define('GITLAB_AUTH', false);
    defined('GITHUB_AUTH') || define('GITHUB_AUTH', false);
    defined('GOOGLE_AUTH') || define('GOOGLE_AUTH', false);

    $container = new Pimple\Container;
    $container->register(new Kanboard\ServiceProvider\HelperProvider());
    $container->register(new Kanboard\ServiceProvider\DatabaseProvider());
    $container->register(new Kanboard\ServiceProvider\ClassProvider());
    $container->register(new Kanboard\ServiceProvider\SessionProvider());
    $container->register(new Kanboard\ServiceProvider\AuthenticationProvider());

    return $container;
}

/**
 * Extract the account name from a string.
 *
 * Example URL format: user.framaboard.org -> extract "user".
 *
 * @param $string the string to parse
 * @return the account name if found, else false
 */
function get_account_name($string) {
    return substr($string, 0, strpos($string, '.'));
}

/**
 * Return the main domain level of a URL.
 *
 * E.g. get_main_domain('example.com') will return 'example.com'
 *      get_main_domain('foo.example.com') will return 'example.com'
 *
 * @param string $domain The domain to extract the main domain.
 * @return string The corresponding main domain.
 */
function get_main_domain($domain) {
    while (substr_count($domain, '.') > 1) {
        $domain = substr($domain, strpos($domain, '.') + 1);
    }
    return $domain;
}

/**
 * Return the list of all the accounts.
 *
 * @return array
 */
function list_accounts()
{
    $list_dirs = array_diff(scandir(PATH_ACCOUNTS), array('..', '.'));
    $accounts = array();
    foreach ($list_dirs as $dir) {
        if (!is_dir(join_path(PATH_ACCOUNTS, $dir))) {
            continue;
        }

        $accounts[] = array(
            'name' => $dir,
        );
    }
    return $accounts;
}

/**
 * Return a list of accounts matching with the given filter.
 *
 * @param string $filter
 *
 * @return array
 */
function filter_accounts($filter)
{
    $list_dirs = array_diff(scandir(PATH_ACCOUNTS), array('..', '.'));
    $accounts = array();
    foreach ($list_dirs as $dir) {
        if (!is_dir(join_path(PATH_ACCOUNTS, $dir)) || !preg_match('/' . $filter . '/i', $dir)) {
            continue;
        }

        $accounts[] = array(
            'name' => $dir
        );
    }
    return $accounts;
}

define('PATH_ROOT', dirname(__FILE__));
define('PATH_ACCOUNTS', join_path(PATH_ROOT, 'users'));
define('PATH_LOST_ACCOUNT_EMAILS', join_path(PATH_ROOT, 'lost_account_emails.txt'));

// Uncomment if you use the configuration provided for Nginx
// $_SERVER['PHP_SELF'] = '/index.php';

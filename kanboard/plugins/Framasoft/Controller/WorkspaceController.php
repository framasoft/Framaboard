<?php

namespace Kanboard\Plugin\Framasoft\Controller;

/**
 * Allow to manage the Framaboard workspaces.
 *
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 */
class WorkspaceController extends \Kanboard\Controller\BaseController
{
    /**
     * Display the deletion form
     */
    public function manage($errors = [])
    {
        $this->response->html(
            $this->helper->layout->config('Framasoft:workspace/manage', [
                'title' => t('Settings') . ' &gt; Gestion espace Framaboard',
                'errors' => $errors,
            ])
        );
    }

    /**
     * Delete the Framaboard workspace and redirect to home
     */
    public function delete()
    {
        $user = $this->getUser();
        $values = $this->request->getValues();
        $values['username'] = $user['username'];
        list($valid, $errors) = $this->authValidator->validateForm($values);

        if ($valid && PATH_ACCOUNT) {
            rm_rf(PATH_ACCOUNT); // BOOM!
            return $this->response->redirect('//' . URL_BASE);
        } else {
            return $this->manage($errors);
        }
    }
}

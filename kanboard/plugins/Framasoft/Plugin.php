<?php

namespace Kanboard\Plugin\Framasoft;

use Kanboard\Core\Plugin\Base;

class Plugin extends Base
{
    public function initialize()
    {
        $this->setContentSecurityPolicy(array(
            'default-src' => "'self' https://framasoft.org/ https://status.framasoft.org/ https://docs.framasoft.org/ https://contact.framasoft.org/ 'unsafe-inline'",
            'script-src' => "'self' https://framasoft.org https://stats.framasoft.org/ 'unsafe-inline'",
            'font-src' => "'self' https://framasoft.org/",
            'img-src' => '* data:',
        ));

        $this->hook->on('template:layout:js', array('template' => 'plugins/Framasoft/js/fix.js'));
        $this->hook->on('template:layout:css', array('template' => 'plugins/Framasoft/css/fix.css'));
        $this->template->hook->attach('template:layout:head', 'Framasoft:layout/nav');
        $this->template->hook->attach('template:auth:login-form:after', 'Framasoft:auth/use-username');
        $this->template->hook->attach('template:config:sidebar', 'Framasoft:config/sidebar');

        $this->applicationAccessMap->add('WorkspaceController', '*', \Kanboard\Core\Security\Role::APP_ADMIN);
    }

    public function getPluginName() {
        return 'Framasoft';
    }

    public function getPluginAuthor() {
        return 'Marien Fressinaud';
    }

    public function getPluginVersion() {
        return '1.0';
    }

    public function getPluginDescription() {
        return '"Fizze" le thème de Kanboard';
    }

    public function getPluginHomepage() {
        return 'https://framagit.org/framasoft/Framaboard';
    }
}

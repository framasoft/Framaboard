<script type="text/javascript">
    // Create a paragraph to explain to use username and not email address,
    // hidden by default.
    var lisezBordel = document.createElement('p');
    var lisezBordelContent = document.createTextNode(
        'Veuillez saisir votre identifiant et non votre adresse email'
    );
    lisezBordel.appendChild(lisezBordelContent);
    lisezBordel.id = 'lisez-bordel';
    lisezBordel.className = 'alert alert-warning';
    lisezBordel.style.display = 'none';

    // Insert the paragraph after the username input (in DOM-world, it's after
    // the required ("*") element.
    var usernameInput = document.getElementById('form-username');
    var requiredInfo = usernameInput.nextElementSibling;
    requiredInfo.parentNode.insertBefore(lisezBordel, requiredInfo.nextSibling);

    // Listen on username input change to check if user insert a '@'. If it's
    // the case, display the paragraph.
    usernameInput.addEventListener('keyup', function() {
        if (usernameInput.value.indexOf('@') !== -1) {
            lisezBordel.style.display = 'block';
        } else {
            lisezBordel.style.display = 'none';
        }
    });
</script>

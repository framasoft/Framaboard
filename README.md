[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# Framaboard

Ce dépot contient le code qui permet de faire tourner Framaboard.

Framaboard est une application de gestion de tâches basée sur [le logiciel Kanboard](https://github.com/kanboard/kanboard) de [Frédéric Guillot](https://github.com/fguillot). Il se base principalement sur l'ajout de deux fichiers spécifiques : `kanboard/config.php` (le fichier de configuration) et `framaboard/index.php` qui permet à des utilisateurs de s'enregistrer pour obtenir leur propre instance de Kanboard.

Si Framaboard n'utilise pas la possibilité qu'offre Kanboard de faire du multi-utilisateurs, c'est pour plusieurs raisons :

- Nous séparons les bases de données des utilisateurs ;
- Il est plus facile de repérer un abus ;
- La sauvegarde est facilitée pour les utilisateurs.

## Version supportée de Kanboard

La dernière version testée et utilisée avec Framaboard est Kanboard 1.0.47.

## Fonctionnement

Framaboard utilise une configuration spécifique pour Kanboard. Nous prendrons comme exemple le site http://board.exemple.

- http://board.exemple correspond à la page d'accueil de Framaboard, celle sur laquelle un utilisateur peut créer son compte. Par exemple, le compte "toto" ;
- Une fois le compte créé, le nouveau board est accessible grace à un sous-domaine. Par exemple, l'adresse http://toto.board.exemple ;
- Comme il n'existe qu'une seule instance de Kanboard installée, c'est le fichier `./kanboard/config.php` qui se charge ensuite de déterminer quelle base de données doit être chargée ;
- Les bases de données et fichiers des différents comptes se trouvent dans le répertoire `./users`.

## Développement

### Pré-requis

Vous devriez installer [Docker](https://docs.docker.com/engine/installation/) et [docker-compose](https://docs.docker.com/compose/install/).

### Étapes

Vous devez commencer par installer Kanboard :

1. Téléchargez [la version souhaitée de Kanboard](https://github.com/kanboard/kanboard/releases) ;
2. Dézippez l'archive dans le répertoire `./kanboard` ;

Ensuite, il vous faudra modifier votre fichier `/etc/hosts` pour que votre machine redirige les sous-domaines de `localhost` sur votre machine. Cela devrait donner quelque chose comme ça :

```console
$ cat /etc/hosts
127.0.0.1   localhost *.localhost
::1         localhost *.localhost
```

Pour éditer le fichier, il vous faudra avoir les droits root. Si vous utilisez Nano comme éditeur de texte par exemple, vous pouvez faire :

```console
$ sudo nano /etc/hosts
```

Vous n'avez plus qu'à lancer l'application avec la commande suivante :

```console
$ make start
```

Cela démarre `docker-compose`, avec la configuration présente dans le fichier [`docker/docker-compose.yml`](docker/docker-compose.yml). Tout est normalement correctement configuré et vous n'avez rien d'autre à faire. Vous pouvez accéder à Framaboard à l'adresse [localhost:8000](http://localhost:8000).

## Production

### Pré-requis

Comme expliqué plus haut, Framaboard se base sur des sous-domaines pour gérer les différents comptes. Aussi, il est nécessaire qu'un enregistrement DNS "wildcard" pointe sur votre serveur. Par exemple :

```
* IN CNAME board.exemple.
```

De plus, il est nécessaire de configurer votre serveur web correctement :

- Faites pointer l'adresse board.exemple sur le répertoire `./framaboard` ;
- Faites pointer **toutes** les adresses `*.board.exemple` sur le répertoire `./kanboard`.

Vous trouverez des exemples pour Apache et Nginx plus loin dans ce README.

### Étapes

Il ne reste plus que quelques étapes avant d'avoir un Framaboard qui fonctionne :

1. Téléchargez [la version souhaitée de Kanboard](https://github.com/kanboard/kanboard/releases) ;
2. Dézippez l'archive dans le répertoire `./kanboard` ;
3. Mettez le tout sur votre serveur ;
4. Assurez-vous que le serveur possède les droits en écriture sur `./users` et `./kanboard/data` ;
5. Éditez les fichiers `./common.php` (pour changer la valeur de `URL_BASE`) et `./kanboard/config.php` (principalement pour `MAIL_FROM`) ;
6. Vous avez fini !

Fini ne signifie pas sécurisé ! Pensez à mettre une authentification HTTP sur le répertoire `./framaboard/admin` sans quoi n'importe qui pourrait supprimer des comptes.

## Configuration serveur

Voici des *exemples* de fichiers de configuration. Ils peuvent (et doivent !) être adaptés à votre situation.

### Apache

```
<VirtualHost *:80>
    ServerAdmin contact@your-server.tld
    DocumentRoot /var/www/html/Framaboard/framaboard
    ServerName your-server.tld

    <Directory /var/www/html/Framaboard/framaboard>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

<VirtualHost *:80>
    ServerAdmin contact@your-server.tld
    DocumentRoot /var/www/html/Framaboard/kanboard
    # Le ServerName doit être différent de your-server.tld (qui a
    # déjà été indiqué plus haut) ou sinon vous allez faire face à de
    # petits soucis. Mettez donc ce que vous voulez :).
    ServerName something.your-server.tld
    ServerAlias *.your-server.tld

    <Directory /var/www/html/Framaboard/kanboard>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

```

### Nginx

Vous pouvez vous inspirer du fichier [`docker/nginx.conf`](docker/nginx.conf)
pour Nginx. Vous aurez probablement à changer les éléments suivants :

- `listen` devra probablement écouter sur le port 80
- `server_name` doit correspondre à votre nom de domaine
- `root` doit pointer sur les bons répertoires (faites attention au chemin !)
- `fast_cgi_path` pointe vers le service `php` définit par docker-compose, vous
  aurez peut-être besoin de faire pointer celui-ci sur `127.0.0.1` ou sur une
  socket unix (ex. `unix:/var/run/php7-fpm.sock`)

Attention, les valeurs sont dupliquées entre deux blocs `server`, pensez à
éditer les deux.

## Mise à jour de Kanboard

Régulièrement, il est nécessaire de mettre à jour de Kanboard pour suivre ses
évolutions. La tâche n'est pas encore facile car Framaboard se base beaucoup
sur les API internes de Kanboad, API qui évoluent à chaque nouvelle version. Il
est donc encore nécessaire de bien tester la création des comptes ainsi que la
connexion automatique après création et la mise à jour du mot de passe à partir
de l'administration.

Pour cela, rien de magique : il faut tester ces différentes fonctionnalités
avec les logs sous les yeux et adapter le code (d'expérience, principalement
dans `./common.php`).

Le ticket [#2698](https://github.com/kanboard/kanboard/issues/2698) sur le
bugtracker de Kanboard est censé répondre à cette problématique en introduisant
dans l'API **externe** de Kanboard les mécanismes nécessaires à la création
d'un compte.

Il n'existe pas aujourd'hui de tests pour assurer le bon fonctionnement de
l'application.

Il est aussi nécessaire de vérifier la bonne intégration de la Framanav avec la
nouvelle version de Kanboard. Un peu n'importe quoi peut se casser comme le
design de boutons, les modales qui sont décalées ou encore le header qui fait
n'importe quoi. Il est souvent nécessaire de faire des ajustements dans le
fichier [board.css](https://git.framasoft.org/framasoft/framanav/blob/master/ext/board.css).

## Mise à jour de Framaboard

Toutes les informations pour mettre à jour le service Framaboard sont
disponibles sur le wiki de l'asso.
